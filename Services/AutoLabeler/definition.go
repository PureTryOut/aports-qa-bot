// SPDX-FileCopyrightText: 2020-2021 Leo <thinkabit.ukim@gmail.com>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoLabeler

import (
	"errors"
	"fmt"
	"regexp"
	"strings"

	"github.com/rs/zerolog"
	"github.com/xanzy/go-gitlab"
	"gitlab.alpinelinux.org/alpine/infra/aports-qa-bot/MergeRequest"
)

// isUpgrade checks whether the values of the pkgver variable in an APKBUILD have changed
func isUpgrade(
	diff string,
) bool {
	match, _ := regexp.MatchString("\n\\+pkgver\\=", diff)
	if match {
		match, _ := regexp.MatchString("\n-pkgver\\=", diff)
		if match {
			return true
		}
	}
	return false
}

// validRepoNames holds the names of all valid names for repositories
var validRepoNames = []string{"main", "community", "testing", "unmaintained", "non-free"}

// RepoPath holds Repository and PackageDir and APKBUILD. Repository is the name of the
// repository in which the package resides which is acquired by looking at the second to
// last directory component, PackageDir is the directory in which the APKBUILD resides and
// is the last directory component and APKBUILD is a boolean that indicates whether the path
// has APKBUILD as the last component
type RepoPath struct {
	Repository string
	PackageDir string
	IsAPKBUILD bool
}

// contains gets a string and an array of strings to compare to
func contains(needle string, haystack []string) bool {
	for _, x := range haystack {
		if x == needle {
			return true
		}
	}
	return false
}

// ParsePath fills a RepoPath struct with its proper values, it takes one string that must be
// the path and parses it
func ParsePath(path string) (RepoPath, error) {
	result := RepoPath{}

	// We can be given an empty path in cases where a new file is added
	// and we are given OldPath and when a file is removed and NewPath
	// is given
	if path == "" {
		return result, nil
	}

	// Catch any instance that has more than 1 backslash and replace it with only one, then
	// split the string
	re := regexp.MustCompile(`//+`)
	TreatedPath := re.ReplaceAllString(path, "/")
	re = regexp.MustCompile("^/")
	TreatedPath = re.ReplaceAllString(TreatedPath, "")
	re = regexp.MustCompile("/$")
	SplitPath := strings.Split(re.ReplaceAllString(TreatedPath, ""), "/")

	// We need at least 3 components in our path, if there are more then it doesn't
	// really matter we just take the last 3
	if len(SplitPath) < 3 {
		return result, errors.New("not enough elements")
	}

	if SplitPath[len(SplitPath)-1] == "APKBUILD" {
		result.IsAPKBUILD = true
	} else {
		result.IsAPKBUILD = false
	}

	result.PackageDir = SplitPath[len(SplitPath)-2]

	if contains(SplitPath[len(SplitPath)-3], validRepoNames) {
		result.Repository = SplitPath[len(SplitPath)-3]
	} else {
		return result, errors.New("repository name isn't valid")
	}
	return result, nil
}

type Service struct {
	dryRun       bool
	gitlabClient *gitlab.Client
}

func New(dryRun bool, gitlabClient *gitlab.Client) Service {
	return Service{
		dryRun:       dryRun,
		gitlabClient: gitlabClient,
	}
}

var actions = map[MergeRequest.Action]bool{
	MergeRequest.Reopen: true,
	MergeRequest.Open:   true,
	MergeRequest.Update: true,
}

var states = map[MergeRequest.State]bool{
	MergeRequest.Opened: true,
}

func (Service) GetActions() map[MergeRequest.Action]bool {
	return actions
}

func (Service) GetStates() map[MergeRequest.State]bool {
	return states
}

func (s Service) Process(payload *gitlab.MergeEvent, log *zerolog.Logger) {
	// Create a logger we can use with all the information we need
	sLog := log.With().
		Str("service", "AutoLabeler").
		Logger()

	sLog.Info().Msg("starting")

	res, _, err := s.gitlabClient.MergeRequests.GetMergeRequestChanges(
		payload.ObjectAttributes.TargetProjectID,
		payload.ObjectAttributes.IID,
		&gitlab.GetMergeRequestChangesOptions{},
	)
	if err != nil {
		sLog.Error().Msg("failed to get latest changes")
	}

	var AddLabels, RemoveLabels gitlab.Labels

	// Since we don't return on error, this can be a nil, in which case we
	// should absolutely check if the returned value is a nil least we
	// cause a segfault by trying to range over a field of a nil struct
	if res != nil {
		var HasMove, HasRemove, HasNew, HasUpgrade bool
		for _, change := range res.Changes {
			OldParsedPath, cErr := ParsePath(change.OldPath)
			if cErr != nil {
				sLog.Warn().
					Err(cErr).
					Str("old path", change.OldPath).
					Msg("failed to parse old path of the changeset")
				continue
			}
			NewParsedPath, cErr := ParsePath(change.NewPath)
			if cErr != nil {
				sLog.Warn().
					Err(cErr).
					Str("new path", change.NewPath).
					Msg("failed to parse new path of the changeset")
				continue
			}

			// We only label depending on changes in the APKBUILD
			if !OldParsedPath.IsAPKBUILD && !NewParsedPath.IsAPKBUILD {
				continue
			}

			if change.RenamedFile &&
				NewParsedPath.IsAPKBUILD &&
				OldParsedPath.Repository != NewParsedPath.Repository {
				HasMove = true
			}
			if change.DeletedFile {
				HasRemove = true
			}
			if change.NewFile {
				HasNew = true
			}
			// GitLab API split changes on a per-file basis so it is ok to assume
			// that if the APKBUILD component of OldParsedPath and NewParsedPath
			// are set we are definitively dealing with an APKBUILD
			if OldParsedPath.IsAPKBUILD && NewParsedPath.IsAPKBUILD {
				HasUpgrade = isUpgrade(change.Diff)
			}

			if !MergeRequest.ContainsLabel("aports:move", payload.Labels) && HasMove && !ContainsString("aports:move", AddLabels) {
				AddLabels = append(AddLabels, "aports:move")
			} else if MergeRequest.ContainsLabel("aports:move", payload.Labels) && !HasMove && !ContainsString("aports:move", RemoveLabels) {
				RemoveLabels = append(RemoveLabels, "aports:move")
			}
			if !MergeRequest.ContainsLabel("aports:remove", payload.Labels) && HasRemove && !ContainsString("aports:remove", AddLabels) {
				AddLabels = append(AddLabels, "aports:remove")
			} else if MergeRequest.ContainsLabel("aports:remove", payload.Labels) && !HasRemove && !ContainsString("aports:remove", RemoveLabels) {
				RemoveLabels = append(RemoveLabels, "aports:remove")
			}
			if !MergeRequest.ContainsLabel("aports:add", payload.Labels) && HasNew && !ContainsString("aports:add", AddLabels) {
				AddLabels = append(AddLabels, "aports:add")
			} else if MergeRequest.ContainsLabel("aports:add", payload.Labels) && !HasNew && !ContainsString("aports:add", RemoveLabels) {
				RemoveLabels = append(RemoveLabels, "aports:add")
			}
			if !MergeRequest.ContainsLabel("aports:upgrade", payload.Labels) && HasUpgrade && !ContainsString("aports:upgrade", AddLabels) {
				AddLabels = append(AddLabels, "aports:upgrade")
			} else if MergeRequest.ContainsLabel("aports:upgrade", payload.Labels) && !HasUpgrade && !ContainsString("aports:upgrade", RemoveLabels) {
				RemoveLabels = append(RemoveLabels, "aports:upgrade")
			}
		}
	}

	if strings.Contains(payload.ObjectAttributes.TargetBranch, "-stable") {
		version := strings.Split(payload.ObjectAttributes.TargetBranch, "-")
		AddLabels = append(AddLabels, fmt.Sprintf("v%s", version[0]))
	}

	if len(AddLabels) > 0 {
		sLog.Info().
			Str("labels", strings.Join(AddLabels, ", ")).
			Msg("adding labels")
	}
	if len(RemoveLabels) > 0 {
		sLog.Info().
			Str("labels", strings.Join(RemoveLabels, ", ")).
			Msg("removing labels")
	}

	if !s.dryRun && (len(AddLabels) != 0 || len(RemoveLabels) != 0) {
		_, _, err = s.gitlabClient.MergeRequests.UpdateMergeRequest(
			payload.ObjectAttributes.TargetProjectID,
			payload.ObjectAttributes.IID,
			&gitlab.UpdateMergeRequestOptions{
				AddLabels:    &AddLabels,
				RemoveLabels: &RemoveLabels,
			})
		if err != nil {
			sLog.Error().
				Err(err).
				Msg("failed to update labels")
			return
		}
	}

	sLog.Info().Msg("finished")
}
