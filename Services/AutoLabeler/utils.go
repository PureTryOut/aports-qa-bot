// SPDX-FileCopyrightText: 2020 Rasmus Thomsen <oss@cogitri.dev>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

package AutoLabeler

func ContainsString(needle string, haystack []string) bool {
	for _, x := range haystack {
		if needle == x {
			return true
		}
	}
	return false
}
